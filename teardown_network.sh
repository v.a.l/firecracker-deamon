#!/bin/bash
sudo ip link del "$TAP_DEV" 2> /dev/null || true
sudo iptables -D FORWARD -i $TAP_DEV -o enp4s0 -j ACCEPT || true
