import { Registry } from "./registry";
import { Image } from "./image";
import { Firecracker } from "./firecracker";

(async () => {
  const registry = new Registry(
    "https://registry-1.docker.io",
    "https://auth.docker.io",
    "registry.docker.io",
  );
  const image = new Image("node", "latest", registry);
  await image.pull();
  const fc = new Firecracker("1234", "172.16.0.2", image);
  await fc.setup(
    "./vmlinux.5.10.bin",
    "console=ttyS0 reboot=k panic=1 pci=off init=/fly/init",
  );
  // const fc2 = new Firecracker("1235", "172.16.0.3");
  // await fc2.setup(
  //   "./vmlinux.bin",
  //   "console=ttyS0 reboot=k panic=1 pci=off",
  //   "./ubuntu-18.04.ext4",
  // );
  setTimeout(async () => {
    await fc.start();
    // await fc2.start();
  }, 100);
})();
