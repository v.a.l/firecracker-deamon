import { Image } from "./image";
import { ImageConfig } from "./registry";

import { spawn, ChildProcess } from "node:child_process";
import fs, { rm, mkdir, writeFile } from "node:fs/promises";

import axios, { Axios } from "axios";
import { awaitWithTimeout, exists, spawnAsync } from "./utils";
import { config as dconfig } from "./config";
import path from "node:path/posix";

const oneLowerIPv4 = (ip: string) => {
  const arr = ip.split(".").map((o) => parseInt(o));
  arr[3] -= 1;
  return arr.map((v) => v.toString()).join(".");
};

export class Firecracker {
  sockPath!: string;
  vsockPath!: string;
  process!: ChildProcess;
  h!: Axios;
  mac: string;

  onShutdownPromise?: Promise<number>;

  constructor(public id: string, public ip: string, public image: Image) {
    this.mac =
      "06:00:" +
      this.ip
        .split(".")
        .map((i) => parseInt(i).toString(16).padStart(2, "0"))
        .join(":");
  }

  async setup(kernel: string, bootArgs: string) {
    this.sockPath = `./socks/${this.id}.sock`;
    this.vsockPath = `./socks/${this.id}.vsock`;
    if (await exists(this.sockPath)) {
      await rm(this.sockPath);
    }
    if (await exists(this.vsockPath)) {
      await rm(this.vsockPath);
    }
    if (!(await exists("./socks"))) {
      await mkdir("./socks");
    }
    this.process = spawn("firecracker", ["--api-sock", this.sockPath], {
      stdio: "ignore",
    });
    this.process.on("exit", this.onExit);

    this.onShutdownPromise = new Promise((resolve) => {
      this.process.on("exit", (code) => {
        resolve(code ?? 0);
      });
    });

    this.h = axios.create({
      socketPath: this.sockPath,
    });
    await spawnAsync("bash", ["setup_network.sh"], {
      env: {
        ...process.env,
        TAP_DEV: `tap${this.id}`,
        TAP_IP: oneLowerIPv4(this.ip),
        MASK_SHORT: "/30",
      },
    });
    await this.h.put("http://localhost/boot-source", {
      kernel_image_path: kernel,
      boot_args: bootArgs,
    });

    await this.buildInitDisk(this.image.config!);

    await this.h.put("http://localhost/drives/rootfs", {
      drive_id: "rootfs",
      path_on_host: path.join(
        dconfig.cacheDir,
        "image_cache",
        this.id + "_init.img",
      ),
      is_root_device: true,
      is_read_only: false,
    });

    await this.h.put("http://localhost/drives/imagefs", {
      drive_id: "imagefs",
      path_on_host: this.image.imagePath,
      is_root_device: false,
      is_read_only: false,
    });

    console.log("mac", this.mac);
    await this.h.put("http://localhost/network-interfaces/net1", {
      iface_id: "net1",
      guest_mac: this.mac,
      host_dev_name: `tap${this.id}`,
    });

    await this.h.put("http://localhost/vsock", {
      guest_cid: 3,
      uds_path: this.vsockPath,
    });

    await this.h.put("http://localhost/machine-config", {
      vcpu_count: 4,
      mem_size_mib: 4096,
    });
  }

  async start() {
    await this.h.put("http://localhost/actions", {
      action_type: "InstanceStart",
    });
  }

  async stop() {
    if (!this.onShutdownPromise) {
      return;
    }
    await this.h.put("http://localhost/actions", {
      action_type: "SendCtrlAltDel",
    });
    await awaitWithTimeout(this.onShutdownPromise, 3000);
    if (this.process.exitCode === null) {
      this.process.kill();
      await awaitWithTimeout(this.onShutdownPromise, 1000);
    }
    if (this.process.exitCode === null) {
      this.process.kill(9);
    }
  }

  async teardown() {
    await spawnAsync("bash", ["setup_network.sh"], {
      env: {
        ...process.env,
        TAP_DEV: `tap${this.id}`,
        TAP_IP: oneLowerIPv4(this.ip),
        MASK_SHORT: "/30",
      },
    });
  }

  onExit = async (status: number | null) => {
    await this.teardown();
  };

  async buildInitDisk(config: ImageConfig) {
    const flyConfig = {
      ImageConfig: config.config,
      IPConfigs: [
        {
          Gateway: oneLowerIPv4(this.ip),
          Mask: 30,
          IP: this.ip,
        },
      ],
      Tty: true,
      Hostname: this.id,

      EtcResolv: { Nameservers: ["8.8.8.8"] },
    };

    const imageCache = path.join(dconfig.cacheDir, "image_cache");
    const initFilename = path.join(imageCache, this.id + "_init.img");
    const initWorkdir = path.join(
      dconfig.cacheDir,
      "workdirs",
      this.id + "_init/",
    );

    if (!(await exists(imageCache))) {
      await mkdir(imageCache);
    }

    if (await exists(initFilename)) {
      await rm(initFilename);
    }

    await spawnAsync("truncate", ["-s", "200M", initFilename]);
    await spawnAsync("mkfs.ext2", [initFilename]);

    await mkdir(initWorkdir, { recursive: true });

    await spawnAsync("mount", [
      "-o",
      "loop,noatime",
      initFilename,
      initWorkdir,
    ]);

    const flyDir = path.join(initWorkdir, "fly");
    await mkdir(flyDir, { recursive: true });

    await writeFile(path.join(flyDir, "run.json"), JSON.stringify(flyConfig));
    await spawnAsync("cp", ["init", flyDir]);

    await spawnAsync("umount", [initWorkdir]);
  }
}
