export interface Config {
  cacheDir: string;
  kernel: string;
  init: string;

  ipRanges: string[];
}

export const config = {
  cacheDir: process.cwd() + "/caches",
  kernel: "./vmlinux.5.10.bin",
  init: "./init",
};
