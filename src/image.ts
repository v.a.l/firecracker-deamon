import { ImageConfig, ImageManifest, Registry } from "./registry";
import { ARCH } from "./constants";

import fs, { rm, mkdir } from "node:fs/promises";
import { Readable } from "node:stream";
import crypto from "node:crypto";
import path from "node:path/posix";

import tar from "tar";
import { exists, spawnAsync } from "./utils";
import { config } from "./config";

const streamToString = (stream: Readable): Promise<string> => {
  const chunks: Buffer[] = [];
  return new Promise((resolve, reject) => {
    stream.on("data", (chunk) => chunks.push(Buffer.from(chunk)));
    stream.on("error", (err) => reject(err));
    stream.on("end", () => resolve(Buffer.concat(chunks).toString("utf8")));
  });
};

const verifyFile = async (
  digest: string,
  filepath: string,
): Promise<boolean> => {
  const hashType = digest.split(":")[0];
  const providedHash = digest.split(":")[1];
  if (
    !crypto
      .getHashes()
      .map((h) => h.toLowerCase())
      .includes(hashType)
  ) {
    throw new Error(`Unknown hash type: ${hashType}`);
  }
  const hash = crypto.createHash(hashType);
  const fp = await fs.open(filepath, fs.constants.O_RDONLY);

  await new Promise<void>((resolve, reject) => {
    const fstream = fp.createReadStream({ emitClose: true });
    fstream.on("data", (chunk) => {
      hash.update(chunk);
    });
    fstream.on("close", () => resolve());
    fstream.on("error", (e) => reject(e));
  });

  return providedHash === hash.digest("hex");
};

export class Image {
  config?: ImageConfig;
  manifest?: ImageManifest;

  constructor(
    public name: string,
    public tag: string,
    public registry: Registry,
  ) {}

  async pull() {
    const token = await this.registry.getToken(this.name, "pull");
    const manifest = await this.getManifest(token);
    const imageConfig = await this.getImageConfig(
      token,
      manifest.config.digest,
    );

    this.config = imageConfig;
    this.manifest = manifest;

    const cacheDir = path.join(config.cacheDir, "layer_cache");
    if (!(await exists(cacheDir))) {
      await mkdir(cacheDir);
    }

    const dlQueue: string[] = [...manifest.layers.map((l) => l.digest)];
    const seenQueue: string[] = [];
    for (const digest of dlQueue) {
      const filename = path.join(
        cacheDir,
        digest.replace(":", "_") + ".tar.gz",
      );
      if ((await exists(filename)) && (await verifyFile(digest, filename))) {
        continue;
      }

      console.log("downloading", digest);

      const lstream = await this.getLayer(token, digest);

      const fp = await fs.open(
        filename,
        fs.constants.O_CREAT | fs.constants.O_WRONLY,
      );
      const fstream = fp.createWriteStream({
        emitClose: true,
      });

      await new Promise<void>((resolve, reject) => {
        fstream.on("close", () => {
          console.log("done");
          resolve();
        });
        fstream.on("error", (e) => {
          console.error("error", e);
          reject(e);
        });
        lstream.pipe(fstream);
      });

      if (!(await verifyFile(digest, filename))) {
        if (!seenQueue.includes(digest)) {
          dlQueue.push(digest);
          seenQueue.push(digest);
        } else {
          throw new Error(
            `File ${digest} is corrupt. Attempted redownload but it was also corrupt.`,
          );
        }
      }
    }
    // console.log(config);
    await this.createImage();
  }

  async getManifest(
    token: string,
    digest: string = this.tag,
  ): Promise<ImageManifest> {
    const manifest = await this.registry.getManifest(token, this.name, digest);
    // console.log(manifest);
    if (
      manifest.mediaType ===
      "application/vnd.docker.distribution.manifest.list.v2+json"
    ) {
      const archItem = manifest.manifests.find(
        (i) => i.platform.architecture === ARCH,
      );
      if (!archItem) {
        throw new Error(
          `Couldn't find image manifest for this architecture: ${ARCH}`,
        );
      }
      return await this.getManifest(token, archItem.digest);
    }
    return manifest;
  }

  async getImageConfig(token: string, digest: string): Promise<ImageConfig> {
    return JSON.parse(
      await streamToString(
        await this.registry.getBlob(
          token,
          this.name,
          digest,
          "application/vnd.docker.container.image.v1+json",
        ),
      ),
    );
  }

  async getLayer(token: string, digest: string): Promise<Readable> {
    return this.registry.getBlob(
      token,
      this.name,
      digest,
      "application/vnd.docker.image.rootfs.diff.tar.gzip",
    );
  }

  async createImage() {
    if (!this.manifest || !this.config) {
      throw new Error();
    }
    const rootFilename = this.imagePath;
    const rootWorkdir = path.join(
      config.cacheDir,
      "workdirs",
      this.manifest.config.digest.replace(":", "_") + "_root/",
    );
    const size = this.manifest.layers.reduce<number>((p, c) => {
      if (typeof c.size !== "number") {
        return p;
      }
      return p + c.size;
    }, 1000 * 1000 * 1000);

    const imgDir = path.join(config.cacheDir, "image_cache");
    if (!(await exists(imgDir))) {
      await mkdir(imgDir);
    }

    if (await exists(rootFilename)) {
      // TODO remove
      return;
      await rm(rootFilename);
    }

    await spawnAsync("truncate", ["-s", size.toString(), rootFilename]);
    await spawnAsync("mkfs.ext4", [rootFilename]);

    await mkdir(rootWorkdir, { recursive: true });

    await spawnAsync("mount", [rootFilename, rootWorkdir]);

    for (const { digest } of this.manifest.layers) {
      const filename = path.join(
        config.cacheDir,
        "layer_cache",
        digest.replace(":", "_") + ".tar.gz",
      );
      try {
        await tar.x({
          file: filename,
          cwd: rootWorkdir,
        });
      } catch {
        console.error("failed to extract", filename, "... continuing");
      }
    }

    await spawnAsync("umount", [rootWorkdir]);
  }

  get imagePath(): string {
    return path.join(
      config.cacheDir,
      "image_cache",
      this.manifest!.config.digest.replace(":", "_") + "_root.img",
    );
  }
}
