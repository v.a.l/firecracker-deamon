import { stat } from "node:fs/promises";
import { PathLike } from "node:fs";
import { spawn, SpawnOptions } from "node:child_process";

export const exists = async (path: PathLike): Promise<boolean> => {
  try {
    const v = await stat(path);
    return true;
  } catch (e) {
    if (e instanceof Error && "code" in e && e.code === "ENOENT") {
      return false;
    }
    throw e;
  }
};

export const spawnAsync = (
  command: string,
  args: string[],
  options?: SpawnOptions,
): Promise<number> => {
  return new Promise((resolve, reject) => {
    const proc = spawn(
      command,
      args,
      options
        ? {
            ...options,
            stdio: options.stdio ?? ["pipe", "ignore", "pipe"],
          }
        : {
            stdio: ["pipe", "ignore", "pipe"],
          },
    );
    proc.on("exit", (code) => {
      if ((code ?? 0) !== 0) {
        reject(`${command} ${args.join(" ")} exited with code: ${code}`);
        return;
      }
      resolve(0);
    });
  });
};

class TimeoutError extends Error {}
export type AwaitWithTimeoutResult<T> =
  | { timeout: false; value: T }
  | { timeout: true };
export const awaitWithTimeout = async <T>(
  promise: Promise<T>,
  timeout: number,
): Promise<AwaitWithTimeoutResult<T>> => {
  const timeoutPromise = new Promise<T>((_, reject) => {
    setTimeout(() => reject(new TimeoutError()), timeout);
  });
  try {
    const v = await Promise.race([promise, timeout]);
    return { timeout: false, value: v as Awaited<T> };
  } catch (e) {
    if (e instanceof TimeoutError) {
      return { timeout: true };
    }
    throw e;
  }
};
