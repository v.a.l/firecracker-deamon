import axios, { Axios } from "axios";
import { VERSION } from "./constants";

export type ManifestTypes =
  | "application/vnd.docker.distribution.manifest.v2+json"
  | "application/vnd.docker.distribution.manifest.list.v2+json"
  | "application/vnd.docker.distribution.manifest.v1+json";

export interface ImageManifestItem {
  mediaType: ManifestTypes;
  digest: string;
  size: number;
}

export interface ImageManifest {
  mediaType: "application/vnd.docker.distribution.manifest.v2+json";
  schemaVersion: number;
  config: ImageManifestItem;
  layers: ImageManifestItem[];
}

export interface ImageManifestListItem extends ImageManifestItem {
  platform: { architecture: string; os: string; variant?: string };
}

export interface ImageManifestList {
  mediaType: "application/vnd.docker.distribution.manifest.list.v2+json";
  schemaVersion: number;
  manifests: ImageManifestListItem[];
}

export interface ImageConfigHistoryItem {
  created: string;
  created_by: string;
  empty_layer?: boolean;
}

export interface ImageConfig {
  architecture: string;
  config: {
    User: string;
    ExposedPorts: Record<string, object>;
    Env: string[];
    Entrypoint: string[];
    WorkingDir: string;
  };
  created: string;
  history: ImageConfigHistoryItem[];
  os: string;
  rootfs: {
    type: string;
    diff_ids: string[];
  };
}

export class Registry {
  h: Axios;

  constructor(
    public baseUrl: string,
    public authBaseUrl: string,
    public authService: string,
  ) {
    this.h = axios.create({
      baseURL: baseUrl,
      headers: { "User-Agent": `firecracker-daemon/${VERSION}` },
    });
  }

  async getToken(image: string, action: string): Promise<string> {
    const resp = await this.h.get(`${this.authBaseUrl}/token`, {
      params: {
        service: this.authService,
        scope: `repository:${image}:${action}`,
      },
    });
    return resp.data.token;
  }

  async getManifest(
    token: string,
    image: string,
    digest: string,
  ): Promise<ImageManifest | ImageManifestList> {
    const resp = await this.h.get(`/v2/${image}/manifests/${digest}`, {
      headers: {
        Accept: [
          "application/vnd.docker.distribution.manifest.v2+json",
          "application/vnd.docker.distribution.manifest.list.v2+json",
          "application/vnd.docker.distribution.manifest.v1+json",
        ].join(", "),
        Authorization: `Bearer ${token}`,
      },
    });
    return resp.data;
  }

  async getBlob(
    token: string,
    image: string,
    digest: string,
    contentType: string,
  ): Promise<any> {
    const resp = await this.h.get(`/v2/${image}/blobs/${digest}`, {
      headers: {
        Accept: contentType,
        Authorization: `Bearer ${token}`,
      },
      responseType: "stream",
    });
    // console.log(resp.headers);
    return resp.data;
  }
}
